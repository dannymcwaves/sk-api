import request from "supertest";
import app from "../src/app";
import { expect } from "chai";
import faker from "faker";


describe("POST /login", () => {
    it("should return some defined error message with valid parameters", (done) => {
        return request(app).post("/login")
            .field("email", "john@me.com")
            .field("password", "Hunter2")
            .expect(302)
            .end(function(err, res) {
                expect(res.error).not.to.be.undefined;
                done();
            });
    });
});

describe("POST /signup", () => {

	const email = faker.internet.email();
	const password = "randomUser12345";

	console.log(email);

  it("sign up some random user", (done) => {
    return request(app).post("/signup")
      .field("email", email)
      .field("password", password)
      .expect(201)
      .end(function(err, res) {
        // expect(res.error).not.to.undefined;
        console.log(err);
        console.log(res);
        done();
      });
  });
});
