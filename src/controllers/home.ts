
import { Request, Response } from "express";

/**
 * GET /
 * Home page.
 */
export const index = (req: Request, res: Response) => {
    res.status(200).json({
        status: 200,
        message: "[ STATUS ] API SERVER ACTIVE..."
    });
};


export const fourOhfourNotFound = (req: Request, res: Response) => {
    res.status(404);

    // respond with json
    if (req.accepts("json")) {
        res.send({
            error: "NOT FOUND",
            code: 404,
            status: "Not Found",
            message: "Not Found"
        });
        return;
    }

    // default to plain-text. send()
    res.status(404).send("Not found");
};
