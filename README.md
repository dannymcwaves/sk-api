# API
Backend API for sk's bot...

## SETUP
**src/** -- contains the typescript source code.

**test/** -- tests written in jest.

**dist/** -- `npm run build` to create this folder. contains the compiled typescript code


## CONFIG
Not too much configuration needed but you'll need to have a local environment variable file based on the `.env.example` provided.

Run this in the shell to generate one:

```bash
cp .env.example .env
```

There are two mongodb env vars in `.env`;

**MONGODB_URI** is for production and is only used when you set `NODE_ENV=production`

**MONGODB_URI_LOCAL** is for dev or test mode. you'll need a local/remote mongodb instance.


## SCRIPTS
**build** `npm run build` to compile the typescript source code to vanilla JS in the `dist/` folder

**run** `npm start` would compile the typescript source and start the express application.

**watch** `npm run watch` watches for any file changes in the typescript source, compiles it and restarts the express server.

**test** `npm test` runs the jest tests.

Read through the `package.json` to find other scripts available.


## API ENDPOINTS

Most response data format returned from the API server is json-based.

```javascript
// a PING endpoint showing the server is active
app.get("/", homeController.index);

// endpoint for logging in
app.post("/login", userController.postLogin);

// clear sessions and logs out user
app.get("/logout", userController.logout);

// register or signup user endpoint
app.post("/signup", userController.postSignup);

// returns user account information, etc...
app.get("/account", passportConfig.isAuthenticated, userController.getAccount);

// 404 handler
app.use(homeController.fourOhfourNotFound);
```

NB: The post endpoints accept `form-urlencoded` or `json` data types.
